public class FindMin {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);
        int minn;
        if (a < b && a < c) {
            minn = a;
        } else if (b < a && b < c) {
            minn = b;
        } else {
            minn = c;
        }
        System.out.println("MIN:" + minn);


    }
}
